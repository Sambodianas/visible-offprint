module.exports = {
    title: 'Imprensa Suspensa Opus 8',
    description: 'Νε vελ ομνεσ φαβθλασ. Cορπορα cοπιοσαε ρεφορμιδανσ ναμ θτ. Vισ ιν μανδαμθσ ινcορρθπτε. Vιμ ωισι οφφενδιτ ρεφορμιδανσ τε, ατqθι νονθμυ πθτεντ νο qθι, vελ αδ ενιμ vερι φαλλι. Περ πορρο ελειφενδ cθ, δενιqθε μολεστιαε ιδ προ.',
    base: '/visible-offprint/',
    dest: 'public'
}
